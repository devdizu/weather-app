import { SUNNY, SNOW, THUNDER, DRIZZLE, CLOUDY, RAIN } from "../constants/weathers";
import convertUnits from "convert-units";

const getAsyncWeatherData = (api_weather) =>
  new Promise((resolve, reject) => {
    fetch(api_weather)
      .then((response) => response.json())
      .catch((err) => reject(err))
      .then((data) => {
        resolve(transformWeather(data));
      })
      .catch((err) => reject(err));
  });

const getWeatherState = ([weather]) => {
  const { id } = weather;

  if (id < 300) {
    return THUNDER;
  } else if (id < 400) {
    return DRIZZLE;
  } else if (id < 600) {
    return RAIN;
  } else if (id < 700) {
    return SNOW;
  } else if (id === 800) {
    return SUNNY;
  } else {
    return CLOUDY;
  }
};

const transformWeather = (weather_data) => {
  return {
    humidity: weather_data.main.humidity,
    temperature: convertTemp(weather_data.main.temp, "K", "C"),
    weatherState: getWeatherState(weather_data.weather),
    wind: `${weather_data.wind.speed} m/s`,
  };
};

const convertTemp = (data, from, to) => {
  return parseFloat(convertUnits(data).from(from).to(to).toFixed(2));
};

export default getAsyncWeatherData;
