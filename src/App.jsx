import React, { Component } from "react";
import LocationList from "./components/LocationsList";
import "./App.css";

const cities = [
  "Cali,co",
  "New York,us",
  "Buenos Aires,ar",
  "Cape Town,za",
  "Tokio,jp",
];
class App extends Component {
  handleSelectedLocation = (city) => {
    console.log("[EventListener] handleSelectedLocation:", city);
  };

  render() {
    return (
      <div className="App">
        <LocationList
          cities={cities}
          onSelectedLocation={this.handleSelectedLocation}
        ></LocationList>
      </div>
    );
  }
}

export default App;
