import React from "react";
import PropTypes from "prop-types";
import "./styles.scss";

const Location = (props) => {
  return (
    <div className="locationCont">
      <h1>{props.city}</h1>
    </div>
  );
};

// Para definir tipos de parametros en props
Location.propTypes = {
  city: PropTypes.string.isRequired,
};

export default Location;
