import React from "react";
import WeatherIcons from "react-weathericons";
import PropTypes from "prop-types";
import "./styles.scss";

const getWeatherIcon = (weatherState) => {
  const sizeIcon = "4x";
  return (
    <WeatherIcons
      className="wicon"
      name={weatherState ? weatherState : "fire"}
      size={sizeIcon}
    />
  );
};

const WeatherTemperature = ({ temperature, weatherState }) => (
  <div className="weatherTemperatureCont">
    {getWeatherIcon(weatherState)}
    <span className="temparature">{temperature}°</span>
    <span className="temparatureType"> C</span>
  </div>
);

// Para definir tipos de parametros en props
WeatherTemperature.propTypes = {
  temperature: PropTypes.number.isRequired,
  weatherState: PropTypes.string.isRequired,
};

export default WeatherTemperature;
