import React, { Component } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { PropTypes } from "prop-types";
import Location from "./Location";
import WeatherData from "./WeatherData";
import getAsyncWeatherData from "../../services/TransformWeather";
import getUrlWeatherByCity from "../../services/getUrlWeatherByCity";
import "./styles.scss";

class WeatherLocation extends Component {
  constructor(props) {
    super();
    const { city } = props;
    this.state = {
      city,
    };
  }

  // cdm
  componentDidMount() {
    this.loadWeatherData();
  }

  // cdp
  componentDidUpdate(prevProps, prevState) {
  }

  loadWeatherData = async () => {
    const result = await getAsyncWeatherData(
      getUrlWeatherByCity(this.state.city)
    );
    this.setState({
      data: result,
    });
  };

  render() {
    const {onWLClick} = this.props;
    return (
      <div className="weatherLocationCont" onClick={onWLClick}>
        <Location city={this.state.city} />
        {this.state.data ? (
          <WeatherData data={this.state.data} />
        ) : (
          <CircularProgress />
        )}
      </div>
    );
  }
}

// Para definir tipos de parametros en props
WeatherLocation.propTypes = {
  city: PropTypes.string.isRequired
};
export default WeatherLocation;
