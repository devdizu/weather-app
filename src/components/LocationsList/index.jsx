import React, { Component } from "react";
import WeatherLocation from "../WeatherLocation";
import { PropTypes } from "prop-types";

const LocationList = ({ cities, onSelectedLocation }) => {
  const strToComponents = (cities) =>
    cities.map((city, idx) => (
      <WeatherLocation
        key={idx}
        city={city}
        onWLClick={() => onSelectedLocation(city)}
      />
    ));

  return <div>{strToComponents(cities)}</div>;
};
// Para definir tipos de parametros en props
LocationList.propTypes = {
  cities: PropTypes.array.isRequired,
};
export default LocationList;
