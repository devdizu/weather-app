export const SUNNY = "day-sunny";
export const FOG = "day-fog";
export const RAIN = "rain";
export const SNOW = "snow";
export const THUNDER = "thunder";
export const DRIZZLE = "drizzle";
export const CLOUDY = "cloudy";